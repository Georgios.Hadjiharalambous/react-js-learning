import ReactDOM from 'react-dom';
import React from 'react';
import axios from 'axios';




	const testData = [
			{name: "Dan Abramov", avatar_url: "https://avatars0.githubusercontent.com/u/810438?v=4", company: "@facebook"},
      {name: "Sophie Alpert", avatar_url: "https://avatars2.githubusercontent.com/u/6820?v=4", company: "Humu"},
  		{name: "Sebastian Markbåge", avatar_url: "https://avatars2.githubusercontent.com/u/63648?v=4", company: "Facebook"},
	];



class Form extends React.Component{

	userNameInput = React.createRef();
	handleSubmit =async (event) => {
		event.preventDefault();
		console.log(
			this.userNameInput.current.value
		)

		const resp = await axios.get(`https://api.github.com/users/${this.userNameInput.current.value}`)
		//console.log(resp.data)
		this.props.onSubmit(resp.data)
		this.userNameInput.current.value = ""

	};
	render(){
		return(
			<form onSubmit={this.handleSubmit}>
				<input
				type="text"
				placeholder = "Github username"
				ref = {this.userNameInput}
				required
				/>
				<button>Add card</button>
			</form>

		)
	}
}



const CardList = (props)=>(
  <div>
		{props.profiles.map(profile=> <Card key={profile.id} {...profile}/>)}
  </div>

);



class Card extends React.Component{
  render(){

		const profile = this.props;
    return (
      <div className="github_prof" >
        <img src={profile.avatar_url} alt='eee'/>
        <div className="info" >
        <div className = 'name'> {profile.name}</div>
        <div className = 'company'> {profile.company}</div>
        </div>
      </div>

  );
  }
}


class App extends React.Component{

	constructor(props){
		super(props);
		this.state = {
			profiles:[]
		};
	}


	addNewProfile = (profileData) =>{
		console.log(profileData);
		this.setState(prevState=>({profiles:[...prevState.profiles,profileData]}))// [...X,Y] is same with X.concat(Y)
	}
  render(){
    return (
    <div>
      <div className='header'>{this.props.title}</div>
			<Form onSubmit = {this.addNewProfile}/>
      <CardList profiles={this.state.profiles}/>
    </div>
  );
  }
}

ReactDOM.render(
  <App title='The github carda app'/>,
  document.getElementById('root')

)
