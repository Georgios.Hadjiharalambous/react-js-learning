import ReactDOM from 'react-dom';


class App extends React.Component{

  render(){
    return <div className='header'>{this.props.title}</div>;
  }
}

ReactDOM.render(
  <App/>,
  document.getElementById('root')

)
