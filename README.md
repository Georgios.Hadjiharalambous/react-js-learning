Various small projects on React js.
- Github card app  - you can add github usernames and shows their photo and profile etc
- Tic tac toe game
- Star match game

Required to have Node js installed.
## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.




